#!/bin/sh
die () {
	echo "$@" 1>&2
	exit 1
}
test -d autoconf && test -f autoconf/configure.ver && cd autoconf
test -f configure.ver || die "Can't find 'autoconf' dir; please cd into it first"
autoconf --version | egrep '2\.[56][0-9]' > /dev/null
if test $? -ne 0 ; then
  die "Your autoconf was not detected as being 2.5x or 2.6x"
fi

script_dir=$(readlink -f -- $(pwd))
test_llvm_src_root="$script_dir/../../llvm"
test_llvm_obj_root="$script_dir/../../llvm-build"

while true ; do
  read -p "Enter full path to LLVM source root (empty for llvm directory in the same dir level as OpenCRun): " REPLY
  if test -z $REPLY ; then
    REPLY=$test_llvm_src_root
  fi
  if test -d "$REPLY/autoconf/m4" ; then
    llvm_src_root=$REPLY
    llvm_m4=$REPLY/autoconf/m4
    break
  fi
done

while true ; do
  read -p "Enter full path to LLVM object root (empty for llvm-build directory in the same dir level as OpenCRun): " REPLY
  if test -z $REPLY ; then
    REPLY=$test_llvm_obj_root
  fi
  if test -d "$REPLY" ; then
    llvm_obj_root=$REPLY
    break
  fi
done

sed -e "s#^LLVM_SRC_ROOT=.*#LLVM_SRC_ROOT=\"$llvm_src_root\"#" \
    -e "s#^LLVM_OBJ_ROOT=.*#LLVM_OBJ_ROOT=\"$llvm_obj_root\"#" configure.ver > configure.ac
echo "Regenerating aclocal.m4 with aclocal"
rm -f aclocal.m4
aclocal -I $llvm_m4 -I "$llvm_m4/.." || die "aclocal failed"
echo "Regenerating configure with autoconf"
autoconf --warnings=all -o ../configure configure.ac || die "autoconf failed"
cd ..
exit 0
