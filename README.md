OpenCRun with dmmlib
====================

Description
-----------

This is a fork from [the project's official GitHub repository][1]. The purpose
of this repository is to link internally OpenCRun with [dmmlib][2].

OpenCRun build instructions
---------------------------

1.  Clone the required repositories:

    * git clone https://bitbucket.org/joko/opencrun.git
    * git clone http://llvm.org/git/llvm.git
    * cd llvm/tools
    * git clone http://llvm.org/git/clang.git

2.  Use the Git revisions required by OpenCRun:

    * cd clang
    * git checkout a95b9f6dab394fea8378702e1a3d06e3cdfdc9c4
    * cd ../..
    * git checkout 0066f9290e95dddedc47472e927319893929b05b

3.  Apply OpenCRun's patch to clang:

    * cd tools/clang
    * patch -p1 < ../../../opencrun/clang-opencrun.diff

4.  Build and install llvm

    * cd ../../..
    * mkdir llvm-build
    * cd llvm-build
    * ../llvm/configure --enable-optimized (--prefix=/path/to/installation/dir)
    * make
    * make install

5.  Update your PATH enviromental variable (if you used a custom prefix)

6. Build and install OpenCRun

    * cd ../opencrun
    * ./autoconf/AutoRegen.sh
    * cd ..
    * mkdir opencrun-build
    * cd opencrun-build
    * ../opencrun/configure (--prefix=/path/to/installation/dir)
    * make
    * make install

Support
-------

For questions on the OpenCRun it is better to contact with its authors (for
more details please visit [OpenCRun's official repository][1]. In case
something in the building process is not properly working, feel free to contact
[Ioannis Koutras][3].

[1]: https://github.com/speziale-ettore/OpenCRun
[2]: http://dmmlib.microlab.ntua.gr
[3]: mailto:ioannis.koutras@gmail.com
